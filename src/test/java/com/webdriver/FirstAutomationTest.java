package com.webdriver;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class FirstAutomationTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "C:\\Marionette\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testClause() throws Exception {
        driver.get("https://primus.nss.udel.edu/CoursesSearch/");
        driver.findElement(By.id("term")).click();
        new Select(driver.findElement(By.id("term"))).selectByVisibleText("2018 Spring (2183)");
        driver.findElement(By.id("term")).click();
        driver.findElement(By.id("course_number")).click();
        driver.findElement(By.id("course_number")).clear();
        driver.findElement(By.id("course_number")).sendKeys("CISC615");
        driver.findElement(By.id("instr_name")).click();
        driver.findElement(By.id("instr_name")).clear();
        driver.findElement(By.id("instr_name")).sendKeys("Clause,James");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        try {
            assertEquals("Clause,James Alexander", driver.findElement(By.xpath("//table[@id='results-2183']/tbody/tr/td[8]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
