package com.webdriver;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Part2a {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "C:\\Marionette\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testPart2a() throws Exception {
        driver.get("https://www.target.com/");
        assertEquals("Target : Expect More. Pay Less.", driver.getTitle());
        try {
            assertEquals("Target : Expect More. Pay Less.", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertTrue(isElementPresent(By.id("home")));
        try {
            assertTrue(isElementPresent(By.id("home")));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertTrue(isElementPresent(By.id("home")));
        driver.findElement(By.id("search")).click();
        driver.findElement(By.id("search")).clear();
        driver.findElement(By.id("search")).sendKeys("ipad");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        try {
            assertEquals("ipad", driver.findElement(By.id("search")).getAttribute("value"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertEquals("ipad", driver.findElement(By.id("search")).getAttribute("value"));
        try {
            assertEquals("off", driver.findElement(By.id("search")).getAttribute("autocomplete"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertEquals("off", driver.findElement(By.id("search")).getAttribute("autocomplete"));
        try {
            assertEquals("filter results", driver.findElement(By.xpath("//div[@id='mainContainer']/div[4]/div/div/div[2]/div[2]/div/h2")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertEquals("filter results", driver.findElement(By.xpath("//div[@id='mainContainer']/div[4]/div/div/div[2]/div[2]/div/h2")).getText());
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}