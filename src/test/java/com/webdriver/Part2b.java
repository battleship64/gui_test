package com.webdriver;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Part2b {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "C:\\Marionette\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testPart2b() throws Exception {
        driver.get("https://www.yahoo.com/");
        driver.findElement(By.id("uh-search-box")).click();
        driver.findElement(By.id("uh-search-box")).clear();
        driver.findElement(By.id("uh-search-box")).sendKeys("selenium");
        driver.findElement(By.id("uh-search-form")).submit();
        try {
            assertEquals("selenium - Yahoo Search Results", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertTrue(isElementPresent(By.id("logo")));
        try {
            assertTrue(isElementPresent(By.id("logo")));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        try {
            assertEquals("selenium", driver.findElement(By.id("yschsp")).getAttribute("value"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertEquals("selenium", driver.findElement(By.id("yschsp")).getAttribute("value"));
        try {
            assertEquals("both", driver.findElement(By.id("yschsp")).getAttribute("aria-autocomplete"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertEquals("both", driver.findElement(By.id("yschsp")).getAttribute("aria-autocomplete"));
        try {
            assertEquals("Sign in", driver.findElement(By.id("yucs-login_signIn")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertEquals("Sign in", driver.findElement(By.id("yucs-login_signIn")).getText());
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
